<?php
class user_role_capabilities_export_action {

public function Export_Action() {

          $export_columns= array(
            'slug'=>'slug',
            'name' => 'name',
            'capabilities' => 'capabilities',
          );
          $role = get_option('wp_user_roles');
          // print_r($role);
          $custom_data = array();
          $data = array();
          $selective_capability = array();
               foreach ($role as $key1 => $rdata) {
  
                      foreach ($rdata['capabilities'] as $key => $value){
                        if($value==1)
                      $selective_capability[$key]=$value;
                      }
                  $capabilities=  array_keys($selective_capability);
                  $capabilities = implode('|',$capabilities);
                  $data[]=array(
                    'slug' => $key1,
                    'name' => $rdata['name'],
                    'capabilities' => $capabilities,
                  );
                  unset($selective_capability);
              
                }

        $export_to = !empty($_POST['export-to']) ? $_POST['export-to'] : '';
        switch ( $export_to ){
          case 'csv' :  
                    require_once('class-user-role-capabilities-export-to-csv.php');
		            $actions= new user_role_capabilities_export_to_csv;
                $actions->Export_to_csv($data, $export_columns);
                break;
          case 'xml' :
                    require_once('class-user-role-capabilities-export-to-xml.php');
		            $actions= new user_role_capabilities_export_to_xml;
		            $actions->Export_to_xml($data, $export_columns);
                                  
          } 

 
}





}