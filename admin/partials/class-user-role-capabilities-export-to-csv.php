<?php
class user_role_capabilities_export_to_csv {

	public function Export_to_csv($data, $export_columns)
	{

        global $wpdb;
        $wpdb->hide_errors();
        @set_time_limit(0);
        if (function_exists('apache_setenv'))
            @apache_setenv('no-gzip', 1);
        @ini_set('zlib.output_compression', 0);
        @ob_end_clean();
 
         header('Content-Type: text/csv; charset=UTF-8');
         header('Content-Disposition: attachment; filename=Role-exp-' . date('Y_m_d_H_i_s', current_time('timestamp')) . ".csv");
         header('Pragma: no-cache');
         header('Expires: 0');
         $fp = fopen('php://output', 'w');

         $row = array();
         // Export header rows
         foreach ($export_columns as $value) {
                 $row[] = $value;
         }
 
        $row = array_map('user_role_capabilities_import_export_for_wordpress::wrap_column', $row);
         fwrite($fp, implode(',', $row) . "\n");
         unset($row);

         foreach ($data as $value) {
          // print_r($term);
        $custom_data = array();
            foreach ($export_columns as $key) {
                $custom_data[$key] = !empty($value[$key]) ? $value[$key] : '';
            }

             $row = array_map('user_role_capabilities_import_export_for_wordpress::wrap_column', $custom_data);
                 fwrite($fp, implode(',', $row) . "\n");
                 unset($row);
                 unset($custom_data);
    }
    
     
         fclose($fp);
         exit;   
	}

}