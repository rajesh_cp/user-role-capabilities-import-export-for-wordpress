<?php
if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class user_role_capabilities_export_List_Table extends WP_List_Table {

 

  
  public function __construct() {
    // Set parent defaults.
    parent::__construct( array(
      'singular' => 'movie',     // Singular name of the listed records.
      'plural'   => 'movies',    // Plural name of the listed records.
      'ajax'     => false,       // Does this table support ajax?
    ) );
  }



  public function get_data() {
    $data[]=array();
        $role = get_option('wp_user_roles');
        // print_r($role);
        $custom_data = array();
        $selective_capability = array();
      foreach ($role as $key1 => $rdata) {
        $custom_data['slug'] = $key1;
        $custom_data['name'] = $rdata['name'];
      // $cust_key=$rdata['name'];
    
      foreach ($rdata['capabilities'] as $key => $value){
        if($value==1)
       $selective_capability[$key]=$value;
        }
          $capabilities=  array_keys($selective_capability);
          $custom_data['capabilities'] = implode("---",$capabilities);
          unset($selective_capability);
          $data[] = array(
            'ID' => $custom_data['slug'],
            'title' => 1,
            'Role' => $custom_data['name'],
            'Capabilities' => $custom_data['capabilities'],
        );
  
      }
      // print_r( $data);die;
    
    //  foreach ($custom_data as $key => $value) {
     
    //  }

$data = array_filter($data);
    return $data;
  }
 



  public function get_columns() {
    $columns = array(
      'cb'       => '<input type="checkbox" />', // Render a checkbox instead of text.  
      'Role'   => _x( 'Role', 'Column label', 'category-tag-import-export-for-woocommerce' ),
      'Capabilities' => _x( 'Capabilities', 'Column label', 'category-tag-import-export-for-woocommerce' ),
      // 'title'    => _x( 'Term Id', 'Column label', 'category-tag-import-export-for-woocommerce' ),
      // 'Description'    => _x( 'Description', 'Column label', 'category-tag-import-export-for-woocommerce' ),
    );
    return $columns;
  }
  
  protected function get_sortable_columns() {
    $sortable_columns = array(
      'NaRoleme'   => array( 'Role', false ),
      'Capabilities' => array( 'Capabilities', false ),
    );
    return $sortable_columns;
  }
  
  protected function column_default( $item, $column_name ) {
    switch ( $column_name ) {
      case 'Role':
      case 'Capabilities':
        return $item[ $column_name ];
      default:
        return print_r( $item, true ); // Show the whole array for troubleshooting purposes.
    }
  }
 
  protected function column_cb( $item ) {
    return sprintf(
      '<input type="checkbox" name="%1$s[]" value="%2$s" />',
      $this->_args['singular'],  // Let's simply repurpose the table's singular label ("movie").
      $item['ID']                // The value of the checkbox should be the record's ID.
    );
  }
 
  protected function column_title( $item ) {
    $page = wp_unslash( $_REQUEST['page'] ); // WPCS: Input var ok.
    // Build edit row action.
    $edit_query_args = array(
      'page'   => $page,
      'action' => 'edit',
      'movie'  => $item['ID'],
    );
    $actions['edit'] = '';
    // Build delete row action.
    $delete_query_args = array(
      'page'   => $page,
      'action' => 'delete',
      'movie'  => $item['ID'],
    );
    $actions['delete'] = '';
    // Return the title contents.
    return sprintf( '%1$s <span style="color:silver;">(id:%2$s)</span>%3$s',
      $item['title'],
      $item['ID'],
      $this->row_actions( $actions )
    );
  }
  
  protected function get_bulk_actions() {
    $actions = array(
      'Export' => _x( 'Export to csv', 'List table bulk action', 'wp-list-table-example' ),
    );
    return $actions;
  }
  /**
   * Handle bulk actions.
   *
   * Optional. You can handle your bulk actions anywhere or anyhow you prefer.
   * For this example package, we will handle it in the class to keep things
   * clean and organized.
   *
   * @see $this->prepare_items()
   */
  protected function process_bulk_action() {
    // Detect when a bulk action is being triggered.
    if ( 'Export' === $this->current_action() ) {

      $roles=$_REQUEST['movie'];
      $terms=array();
      $term_capability1=array();
      foreach ($roles as $value) {
        // echo $value;die;
        // $value=strtolower($value);
        $term = get_role($value);
        $term_capability= $term->capabilities;
        // print_r($term_capability);die;
        foreach ($term_capability as $key => $value1){
             if($value1==1)
            $term_capability1[$key]=$value1;
           }
                  //  priterm_capability1nt_r($term_capability1);die;

        $term_capability1= array_keys($term_capability1);
        $term_capability1 = implode('|',$term_capability1);
        $terms[]=array(
          'slug'=> $value,
          'name' => $term->name,
          'capabilities' => $term_capability1,
        );

        unset($term_capability1);
      }
// print_r($terms);die;
            $admin_data=array(
              'slug' => 'slug',
              'name' => 'name',
              'capabilities' => 'capabilities',
              
            );
            require_once(plugin_dir_path( __FILE__ ).'../class-user-role-capabilities-export-to-csv.php');
		            $actions= new user_role_capabilities_export_to_csv;
                $actions->Export_to_csv($terms, $admin_data);


    }
  }
  /**
   * Prepares the list of items for displaying.
   *
   * REQUIRED! This is where you prepare your data for display. This method will
   * usually be used to query the database, sort and filter the data, and generally
   * get it ready to be displayed. At a minimum, we should set $this->items and
   * $this->set_pagination_args(), although the following properties and methods
   * are frequently interacted with here.
   *
   * @global wpdb $wpdb
   * @uses $this->_column_headers
   * @uses $this->items
   * @uses $this->get_columns()
   * @uses $this->get_sortable_columns()
   * @uses $this->get_pagenum()
   * @uses $this->set_pagination_args()
   */
  function prepare_items() {
    global $wpdb; //This is used only if making any database queries
    /*
     * First, lets decide how many records per page to show
     */
    $per_page = 10;
    /*
     * REQUIRED. Now we need to define our column headers. This includes a complete
     * array of columns to be displayed (slugs & titles), a list of columns
     * to keep hidden, and a list of columns that are sortable. Each of these
     * can be defined in another method (as we've done here) before being
     * used to build the value for our _column_headers property.
     */
    $columns  = $this->get_columns();
    $hidden   = array();
    $sortable = $this->get_sortable_columns();
    /*
     * REQUIRED. Finally, we build an array to be used by the class for column
     * headers. The $this->_column_headers property takes an array which contains
     * three other arrays. One for all columns, one for hidden columns, and one
     * for sortable columns.
     */
    $this->_column_headers = array( $columns, $hidden, $sortable );
    /**
     * Optional. You can handle your bulk actions however you see fit. In this
     * case, we'll handle them within our package just to keep things clean.
     */
    $this->process_bulk_action();
    /*
     * GET THE DATA!
     * 
     * Instead of querying a database, we're going to fetch the example data
     * property we created for use in this plugin. This makes this example
     * package slightly different than one you might build on your own. In
     * this example, we'll be using array manipulation to sort and paginate
     * our dummy data.
     * 
     * In a real-world situation, this is probably where you would want to 
     * make your actual database query. Likewise, you will probably want to
     * use any posted sort or pagination data to build a custom query instead, 
     * as you'll then be able to use the returned query data immediately.
     *
     * For information on making queries in WordPress, see this Codex entry:
     * http://codex.wordpress.org/Class_Reference/wpdb
     */
    //  $search = @$_POST['cars']?esc_attr($_POST['cars']):"";
    // print_r($search);
    $data = $this->get_data();
    // $data = $data1;
    /*
     * This checks for sorting input and sorts the data in our array of dummy
     * data accordingly (using a custom usort_reorder() function). It's for 
     * example purposes only.
     *
     * In a real-world situation involving a database, you would probably want
     * to handle sorting by passing the 'orderby' and 'order' values directly
     * to a custom query. The returned data will be pre-sorted, and this array
     * sorting technique would be unnecessary. In other words: remove this when
     * you implement your own query.
     */
    usort( $data, array( $this, 'usort_reorder' ) );
    /*
     * REQUIRED for pagination. Let's figure out what page the user is currently
     * looking at. We'll need this later, so you should always include it in
     * your own package classes.
     */
    $current_page = $this->get_pagenum();
    /*
     * REQUIRED for pagination. Let's check how many items are in our data array.
     * In real-world use, this would be the total number of items in your database,
     * without filtering. We'll need this later, so you should always include it
     * in your own package classes.
     */
    $total_items = count( $data );
    /*
     * The WP_List_Table class does not handle pagination for us, so we need
     * to ensure that the data is trimmed to only the current page. We can use
     * array_slice() to do that.
     */
    $data = array_slice( $data, ( ( $current_page - 1 ) * $per_page ), $per_page );
    /*
     * REQUIRED. Now we can add our *sorted* data to the items property, where
     * it can be used by the rest of the class.
     */
    $this->items = $data;
    /**
     * REQUIRED. We also have to register our pagination options & calculations.
     */
    $this->set_pagination_args( array(
      'total_items' => $total_items,                     // WE have to calculate the total number of items.
      'per_page'    => $per_page,                        // WE have to determine how many items to show on a page.
      'total_pages' => ceil( $total_items / $per_page ), // WE have to calculate the total number of pages.
    ) );
  }
  /**
   * Callback to allow sorting of example data.
   *
   * @param string $a First value.
   * @param string $b Second value.
   *
   * @return int
   */
  protected function usort_reorder( $a, $b ) {
    // If no sort, default to title.
    $orderby = ! empty( $_REQUEST['orderby'] ) ? wp_unslash( $_REQUEST['orderby'] ) : 'title'; // WPCS: Input var ok.
    // If no order, default to asc.
    $order = ! empty( $_REQUEST['order'] ) ? wp_unslash( $_REQUEST['order'] ) : 'asc'; // WPCS: Input var ok.
    // Determine sort order.
    $result = strcmp( $a[ $orderby ], $b[ $orderby ] );
    return ( 'asc' === $order ) ? $result : - $result;
  }

} //class