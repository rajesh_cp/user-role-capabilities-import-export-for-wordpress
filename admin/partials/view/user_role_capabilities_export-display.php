
<div class="outer">
    <div class="inner">
        <h3 class="title">
            <?php _e('Export Role in CSV or XML Format:', 'user-role-capabilities-import-export-for-wordpress'); ?></h3>
        <hr>
        <p><?php _e('Export and download your Role/Capability in CSV or XML format.', 'user-role-capabilities-import-export-for-wordpress'); ?>
        </p>
        <?php 
    $url=admin_url().'admin.php?&page=user_role_capabilities_import_export_for_wordpress&tab=export_action';
  ?>
        <form method="post" action=<?php echo $url;?>>
            <table class="form-table" id="griddata">

                
                <tr>
                    <th>
                        <label
                            for="exoprt_to"><?php _e('Export formate', 'user-role-capabilities-import-export-for-wordpress'); ?></label>
                    </th>
                    <td>
                        <input id="csv_export" name="export-to" type="radio" value="csv" checked>
                        <label
                            for="csv_export"><?php _e('CSV', 'user-role-capabilities-import-export-for-wordpress'); ?></label>&nbsp&nbsp&nbsp
                        <input id="xml_export" name="export-to" type="radio" value="xml">
                        <label
                            for="xml_export"><?php _e('XML', 'user-role-capabilities-import-export-for-wordpress'); ?></label>

                    </td>
                </tr>
         
            </table>
            <input name="download" value="true" type="hidden">
            <input type="submit" class="button button-primary" name="submit" value="Export">
        </form>
    </div>
</div>

