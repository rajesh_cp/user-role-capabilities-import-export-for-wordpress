<div class="outer">
    <div class="inner">
        <div class="tool-box bg-white p-20p pipe-view">
            <h3 class="title">
                <?php _e('Import Roll/Capabilities From CSV or XML Format:', 'user-role-capabilities-import-export-for-wordpress'); ?>
            </h3>
            <hr>
            <p><?php _e('Import Roll/Capabilities from CSV or XML format from your computer.You can import Roll/Capability (in CSV or XML format) in to the shop.', 'user-role-capabilities-import-export-for-wordpress'); ?>
            </p>
            <?php 
            $url=admin_url().'admin.php?&page=user_role_capabilities_import_export_for_wordpress&tab=import_action';
            ?>

            <form enctype="multipart/form-data" action=<?php echo $url;?> id="import-upload-form" method="post">

                <table class="form-table">
                    <tbody>
                        
                        <tr>
                            <th>
                                <label
                                    for="import-from"><?php _e('Import formate', 'user-role-capabilities-import-export-for-wordpress'); ?></label>
                            </th>
                            <td>
                                <input id="csv_import" name="import_as" type="radio" value="csv" checked>
                                <label
                                    for="csv_import"><?php _e('CSV', 'user-role-capabilities-import-export-for-wordpress'); ?></label>&nbsp&nbsp&nbsp
                                <input id="xml_import" name="import_as" type="radio" value="xml">
                                <label
                                    for="xml_import"><?php _e('XML', 'user-role-capabilities-import-export-for-wordpress'); ?></label>

                            </td>
                        </tr>
                        <tr>
                            <th>
                                <label
                                    for="import-from"><?php _e('Update data if exists', 'user-role-capabilities-import-export-for-wordpress'); ?></label>
                            </th>
                            <td>
                                <input type="checkbox" name="merge" id="merge" value="update">
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <label
                                    for="upload"><?php _e('Select a file from your computer', 'user-role-capabilities-import-export-for-wordpress'); ?></label>
                            </th>
                            <td>
                                <input type="file" onchange="ValidateSingleInput(this);" id="upload" name="import" size="25" />
                                <?php
                            $bytes = apply_filters('import_upload_size_limit', wp_max_upload_size());
                            $size = size_format($bytes);
                            $upload_dir = wp_upload_dir();
                            ?>
                                <small><?php printf(__('Maximum size: %s'), $size); ?></small>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <input type="submit" class="button button-primary" name="submit" value="Import">
            </form>

        </div>
    </div>
</div>


<?php	
	?>