<div class="display_title">
    <h2><?php _e('Role Capability Import Export', 'user-role-capabilities-import-export-for-wordpress'); ?></h2>

                    <?php
                            if( isset( $_GET[ 'tab' ] ) ) {
                                $active_tab = $_GET[ 'tab' ];
                            } else{
                                $active_tab = 'selective_role_export';
                            }

                            if($_GET['tab'] =='import_action'){
                                $active_tab='import';
                            }
                    ?>

                    <h2 class="nav-tab-wrapper">

                        <a href="?page=user_role_capabilities_import_export_for_wordpress&tab=selective_role_export"
                            class="nav-tab <?php echo $active_tab == 'selective_role_export' ? 'nav-tab-active' : ''; ?>"><?php _e('Role', 'user-role-capabilities-import-export-for-wordpress'); ?></a>
                        <a href="?page=user_role_capabilities_import_export_for_wordpress&tab=export"
                            class="nav-tab <?php echo $active_tab == 'export' ? 'nav-tab-active' : ''; ?>"><?php _e('Export', 'user-role-capabilities-import-export-for-wordpress'); ?></a>
                        <a href="?page=user_role_capabilities_import_export_for_wordpress&tab=import"
                            class="nav-tab <?php echo $active_tab == 'import' ? 'nav-tab-active' : ''; ?>"><?php _e('Import', 'user-role-capabilities-import-export-for-wordpress'); ?></a>
                    </h2>

                     <?php 


         if ( isset ( $_GET['tab'] ) )
            $tab = $_GET['tab'];
         else
           $tab = 'selective_role_export';

        
        switch ( $tab ){
              case 'export' :
                                require_once 'user_role_capabilities_export-display.php'; 
                                 break; 

              case 'export_action' :
                                require_once(plugin_dir_path( __FILE__ ).'../user-role-capabilities-export-action.php');
                                $actions= new user_role_capabilities_export_action;
                                $actions->Export_Action();
                                 break;
             case 'import' :
                                require_once('user-role-capabilities-import-display.php');
                                 break; 

             case 'import_action' :
                                require_once(plugin_dir_path( __FILE__ ).'../class-user-role-capabilities-import-action.php');
                                $actions= new user_role_capabilities_import_action;
                                $actions->Import_Action();
                                 break; 
             case 'selective_role_export' : 
             
                                require_once 'user_role_capabilities_export_List_Table.php';
                                ?>

                                    <form method="GET">
                                        <div class="exprt_list_table">
                                            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
                                            <?php
                                    $myListTable = new user_role_capabilities_export_List_Table();
                                    $myListTable->prepare_items(); 
                                    $myListTable->display();
                                    ?>
                                    </form>
                                </div>
                                <?php

                } 
                    ?>

</div>