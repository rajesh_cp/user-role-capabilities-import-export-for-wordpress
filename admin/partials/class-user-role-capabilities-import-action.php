<?php

class user_role_capabilities_import_action {


public function Import_Action() {
   $import_as = !empty($_POST['import_as']) ? $_POST['import_as'] : '';

   $chk_existing_update = !empty($_POST['merge']) ? $_POST['merge'] : 'not_update';
     global $woocommerce, $wpdb;
     $this->delimiter='';
		 if (!$this->delimiter)
            $this->delimiter = ',';

           if (empty($_POST['file_url'])) {

            $file = wp_import_handle_upload();
            $this->id = (int) $file['id'];         
        } 
        
        if ($this->id)
            $file = get_attached_file($this->id);
          
          if(isset($file['error'])){
            $url=admin_url().'admin.php?&page=user_role_capabilities_import_export_for_wordpress&tab=import';
            wp_redirect( $url );
            exit;
          }
            
	   // Set locale
        $enc = mb_detect_encoding($file, 'UTF-8, ISO-8859-1', true);
        if ($enc)
            setlocale(LC_ALL, 'en_US.' . $enc);
        @ini_set('auto_detect_line_endings', true);
          if($import_as == 'xml')
   {
    $table_display= $this->Export_to_xml_file($file,$chk_existing_update);
   }else{
        // Get headers
        if (( $handle = fopen($file, "r") ) !== FALSE) {

            $row = $raw_headers = array();
            $header = fgetcsv($handle, 0, $this->delimiter);
            while (( $postmeta = fgetcsv($handle, 0, $this->delimiter) ) !== FALSE) {
                foreach ($header as $key => $heading) {
                    if (!$heading)
                        continue;
                    $s_heading = $heading;
                    $row[$s_heading] = ( isset($postmeta[$key]) ) ? $this->format_data_from_csv($postmeta[$key], $enc) : '';
                    $raw_headers[$s_heading] = $heading;
                }
                break;
            }
            
            fclose($handle);
        }


        $file = str_replace("\\", "/", $file);
    
       $start_pos=0;
         $enc = mb_detect_encoding( $file, 'UTF-8, ISO-8859-1', true );
		if ( $enc )
			setlocale( LC_ALL, 'en_US.' . $enc );
		@ini_set( 'auto_detect_line_endings', true );

		$parsed_data = array();
		$raw_headers = array();
		// Put all CSV data into an associative array
		if ( ( $handle = fopen( $file, "r" ) ) !== FALSE ) {

			$header   = fgetcsv( $handle, 0, $this->delimiter );
		    while ( ( $postmeta = fgetcsv( $handle, 0, $this->delimiter ) ) !== FALSE ) {
	            $row = array();
				
	            foreach ( $header as $key => $heading ) {
					$s_heading = strtolower($heading);

					$row[$s_heading] = ( isset( $postmeta[$key] ) ) ? $this->format_data_from_csv( $postmeta[$key], $enc ) : '';

					$raw_headers[ $s_heading ] = $heading;
	            }
	            $parsed_data[] = $row;

	            unset( $postmeta, $row );
	             $position = ftell( $handle );

	      
		    }
		    fclose( $handle );
		}
    
                $category_details = array();
                $table_display = array();
                $capabilitys = array();

                foreach ($parsed_data as $value) {
                 
                  $datas=explode("|",$value['capabilities']);
                  foreach ($datas as $data) {
                   $capabilitys[$data]=true;
                  }
                  $value['capabilities']=$capabilitys;
                  unset($capabilitys);unset($datas);
                  // print_r($value['capabilities']);die;
                	$satus=$this->create_roll($value,$chk_existing_update);
                	$table_display[]=$satus;
       
                }
                  $table_display= @array_filter(array_map('array_filter', $table_display));
              }
                ?>
<br><br>



<table id="import-progress" class="widefat_importer widefat">
    <thead>
        <tr>
            <th class="status">&nbsp;</th>
            <th class="row">Row</th>
            <th class="reason">Category Name</th>
            <!-- <th>User Status</th> -->
            <th class="reason">Status Msg</th>
        </tr>
    </thead>
    <?php
        	$i=0;
          foreach ( $table_display as $status ) {
            ?>
    <tr>
        <td></td>
        <td class="row"><?php echo ++$i; ?></td>
        <td class="reason"><?php echo $status['name']; ?></td>
        <td class="reason"><?php echo $status['status']; ?></td>

    </tr>
    <?php
                                   } 
                                   ?>
    </tr>
    </tbody>
</table>
<?php 
$url=admin_url().'admin.php?page=user_role_capabilities_import_export_for_wordpress&tab=import';
            ?>
<a href=<?php echo $url;?>>Back</a>

<?php 

  

}

   

/**
 * 
 * utf8 encoding functon
 * 
 */


public function format_data_from_csv($data, $enc) {
        return ( $enc == 'UTF-8' ) ? $data : utf8_encode($data);
    }


/**
 * 
 * this function used to store data in db
 * correct formated data and options are receive 
 * status return
 * 
 */


public function create_roll($data,$chk_existing_update) {
  // print_r($data);die;
  $roll=$data['slug'];
  $display_name=$data['name'];
  if($roll=='')
  {
    $roll = $this->create_slug($display_name);
  }
  // print_r($roll);die;
  $add_capabilities=$data['capabilities'];
  // $roll='bbbb';
  $check_existance = get_role($roll);
  if($check_existance=='')
  {
// echo "sucess";
$result = add_role( $roll, $display_name, $add_capabilities );
        if ( null !== $result ) {
          $status = array(
            'name' => $result->name,
            'status' => 'user role created',
          
        );
          // echo "Success: {$result->name} user role created.";
        }
        else {
          echo 'Failure: user role already exists.';
        }
}else{

  if($chk_existing_update=='update'){
      remove_role($roll);  
      $updated = add_role( $roll, $display_name, $add_capabilities );
      // echo "added";die;
      $status = array(
        'name' => $updated->name,
        'status' => 'user role updated',
      
    );
  
  }else{
  $status = array(
    'name' =>$display_name ,
    'status' => 'user role already exists',
  
);    // echo "faild";
  }
}
   return $status ;
   
}





public function Export_to_xml_file($file,$chk_existing_update) {
  require_once 'library/rss_php.php';
  $rss = new rss_php;

   $rss->load($file);

   $items = $rss->getItems(); #returns all rss items
   $data =$items['rss']['channel']['data'];
   // print_r($data);die;
    foreach ($data as $value) {

     $datas=explode("|",$value['capabilities']);
     // print_r($datas);die;
     foreach ($datas as $data) {
      $capabilitys[$data]=true;
     }
     $value['capabilities']=$capabilitys;
     unset($capabilitys);unset($datas);
     // print_r($value['capabilities']);die;
     $satus=$this->create_roll($value,$chk_existing_update);
                 $table_display[]=$satus;
      
               }
return $table_display;
    
}


public function create_slug($slug){
  // echo "hai";die;
    $slug=strtolower($slug);
    $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $slug);
    return $slug; 
  }

}